import React, { Component } from 'react';

import HeaderHimmel from './components/HeaderHimmel';
import BannerHeader from './components/BannerHeader';
import ServiciosModule from './components/Servicios';
import ParallaxModule from './components/Parallax';
import Footer from './components/Footer';
import './App.css';
import './header.css';


class App extends Component {

  constructor () {
    super();
    this.state = {
    };
  }
   


 
render(){

      // RETURN THE COMPONENT
    return (
      <div className="App">
      <div id="mainRow" className="main">
        <HeaderHimmel/>
        <BannerHeader/>
        <ServiciosModule/>
        <ParallaxModule/>
        <Footer clase={'modal-dialog-centered'}/>
      </div>
      {/* footer*/}


      </div>
    );
  }
}

export default App;
