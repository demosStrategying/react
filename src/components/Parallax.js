import React, { Component } from 'react';

class ParallaxModule extends Component {
  constructor () {
    super();
    this.state = {

    };
  }

  render() {
    
       
         
       
       
     return (
		
      <div className="banner-area">
          {/* init desginer */}        

          <div className="container-fluid encaje designer">
          <section id="designer" data-midnight="nav--black">
    <div className="split split--designer">
    <div className="container container--small container--center">
    <header>
    <h1 className="split__title">Diseñador</h1>
</header>
    <blockquote>
    <q>El buen diseño es obvio. El diseño genial es transparente.</q>
    <footer>
    <cite>Joe Sparano</cite>
</footer>
</blockquote>
    <p>
    <span className="smallcaps">Soy un diseñador multidisciplinario</span>que está siempre buscando aprender y mejorar. Ser multidisciplinario me permite planificar como ingeniero, crear como un artista y pensar como un cliente. Mi curiosidad creativa de mantenerme siempre al día me exige adaptarme a una industria en constante evolución sin verme dejado de lado.</p>
    <p>El diseño realmente no es sólo cómo se ve; la clave para enfrentar un problema desafiante a través de la innovación y el pensamiento creativo es encontrar un balance entre los detalles visuales y el valor que genera desde el punto de vista del usuario final.</p>
</div>
    <span className="split__ampersand">&amp;</span>
</div>
</section>
		  </div>
          {/* end desginer */}        
          {/* init developer */}        
          <div className="container-fluid encaje developer">
          <div className="split split--developer">
<div className="container container--small container--center">
<header>
<h1 className="split__title developer">Desarrollador</h1>
</header>
<blockquote>
<q>Cualquier estúpido puede escribir código que una computadora entienda. Los buenos programadores escriben código que los humanos entienden.</q>
<footer>
<cite>Martin Fowler</cite>
</footer>
</blockquote>
<p>
<span className="smallcaps">Una marca es sólo tan exitosa</span>como su implementación; los grandes productos no deberían ser definidos solamente por su diseño, sino también por su funcionalidad y por una gran experiencia de usuario.</p>
<p>Sin embargo eso es sólo la punta del iceberg. El código no debe sólo funcionar, debe ser comprensible y fácilmente modificable. Los desarrolladores tienen que esforzarse por lograr un código limpio, elegante, y eficiente que asegure la sustentabilidad a largo plazo y mantenga su mantenibilidad ―que va más allá de arreglar errores al incluir la necesidad de adaptarse a un entorno continuamente cambiante.</p>
</div>
<span className="split__ampersand">&amp;</span>
</div>
		  </div>
          {/* end developer */}        
		  </div>
    )
  }
  
}

export default ParallaxModule;
