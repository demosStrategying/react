import React, { Component } from 'react';
import logo from './imgs/himmel.png';

class HeaderHimmel extends Component {
  constructor () {
    super();
    this.state = {

    };
  }


  render() {
    return (
			
		  <header id="header">
		    <div className="container main-menu">
		    	<div className="row align-items-center justify-content-between d-flex">
			      <div id="logo">
			        <a href="index.html"><img src={logo} alt="logo" /></a>
			      </div>
			      <nav id="nav-menu-container">
			        <ul className="nav-menu">
			          <li><a href="index.html">Inicio</a></li>
			          <li><a href="about.html">Servicios</a></li>
			          <li><a href="services.html">Tu sitio</a></li>
			          <li><a href="portfolio.html">Clientes</a></li>
			          <li><a href="price.html">Contacto</a></li>
			        </ul>
			      </nav>		
		    	</div>
		    </div>
		  </header>
    )
  }

}

export default HeaderHimmel;
