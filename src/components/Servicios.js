import React, { Component } from 'react';
import img3rdparty from './imgs/servicios/3rdparty.jpg';
import imgAnalytics from './imgs/servicios/analytics.jpg';
import imgBackend from './imgs/servicios/backend.jpg';
import imgCdn from './imgs/servicios/cdn.jpg';
import imgCloud from './imgs/servicios/cloud.jpg';
import imgEcommerce from './imgs/servicios/ecommerce.jpg';
import imgFonts from './imgs/servicios/fonts.jpg';
import imgImagenesresponsive from './imgs/servicios/imagenes-responsive.jpg';
import imgInfo from './imgs/servicios/info.jpg';
import imgLayout from './imgs/servicios/layout.jpg';
import imgPlugins from './imgs/servicios/plugins.jpg';
import imgSeo from './imgs/servicios/seo.jpg';

class ServiciosModule extends Component {
  constructor (props, context) {
    super(props, context);
    this.state = {
        nombre: '',
        numero: []
    };
    this.christian = this.christian.bind(this);
  }

  christian() {
    var tt= JSON.parse('{"nombre": "christian","age":30, "city":"New York"}');
    

      this.state.numero.push(tt);
      
      this.setState({ christian : this.state.numero[0].nombre });
console.log(this.state.christian);

  }
  render() {
    
         
       
       
     return (
		
<div className="banner-area"> 
    <div className="container"> 
        <div className="row"> 

            <div className="col-lg-3 col-md-3 d-flex align-self-end">
                    <div className="card services__himmel mt-5">
                        <img className="card-img-top" src={imgImagenesresponsive} alt="Card cap 1" />
                        <div className="card-body">RWD</div>
                    </div>
                </div>
            <div className="col-lg-3 col-md-3 d-flex align-self-end">
                <div className="card services__himmel mt-5">
                    <img className="card-img-top" src={imgEcommerce} alt="Card cap 2" />
                    <div className="card-body">Comercios</div>
                </div>
            </div>
            <div className="col-lg-3 col-md-3 d-flex align-self-end">
                <div className="card services__himmel mt-5">
                    <img className="card-img-top" src={imgPlugins} alt="Card cap 3" />
                    <div className="card-body">No requiere plugins</div>
                </div>
            </div>
            <div className="col-lg-3 col-md-3 d-flex align-self-end">
                <div className="card services__himmel mt-5">
                    <img className="card-img-top" src={imgAnalytics} alt="Card cap 4" />
                    <div className="card-body">Analytics</div>
                </div>
            </div>
            <div className="col-lg-3 col-md-3 d-flex align-self-end">
                <div className="card services__himmel mt-5">
                    <img className="card-img-top" src={imgFonts} alt="Card cap 5" />
                    <div className="card-body">Typekit & Google Fonts</div>
                </div>
            </div>
            <div className="col-lg-3 col-md-3 d-flex align-self-end">
                <div className="card services__himmel mt-5">
                    <img className="card-img-top" src={img3rdparty} alt="Card cap 6" />
                    <div className="card-body">Integraciones 3rd Party</div>
                </div>
            </div>
            <div className="col-lg-3 col-md-3 d-flex align-self-end">
                <div className="card services__himmel mt-5">
                    <img className="card-img-top" src={imgBackend} alt="Card cap 7" />
                    <div className="card-body">Administrador web</div>
                </div>
            </div>
            <div className="col-lg-3 col-md-3 d-flex align-self-end">
                <div className="card services__himmel mt-5">
                    <img className="card-img-top" src={imgLayout} alt="Card cap 8" />
                    <div className="card-body">Layout Engine</div>
                </div>
            </div>
            <div className="col-lg-3 col-md-3 d-flex align-self-end">
                <div className="card services__himmel mt-5 mb-5">
                    <img className="card-img-top" src={imgSeo} alt="Card cap 9" />
                    <div className="card-body">SEO / SEM</div>
                </div>
            </div>
            <div className="col-lg-3 col-md-3 d-flex align-self-end">
                <div className="card services__himmel mt-5 mb-5">
                    <img className="card-img-top" src={imgInfo} alt="Card cap 10" />
                    <div className="card-body">Atención al cliente</div>
                </div>
            </div>
            <div className="col-lg-3 col-md-3 d-flex align-self-end">
                <div className="card services__himmel mt-5 mb-5">
                    <img className="card-img-top" src={imgCdn} alt="Card cap 11" />
                    <div className="card-body">CDN</div>
                </div>
            </div>
            <div className="col-lg-3 col-md-3 d-flex align-self-end">
                <div className="card services__himmel mt-5 mb-5">
                    <img className="card-img-top" src={imgCloud} alt="Card cap 12" />
                    <div className="card-body">Infraestructura</div>
                </div>
            </div>

        </div> 
    </div>        
</div>
    )
  }
  
}

export default ServiciosModule;
