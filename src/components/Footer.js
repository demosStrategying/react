import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter/*, Input, Label, Form, FormGroup */} from 'reactstrap';

class Footer extends Component {

  constructor (props, context) {
    super(props, context);
    
    this.state = {
        modal: false,
        buttonLabel: 'Boton'
    };
    this.toggle = this.toggle.bind(this);
  }
   
  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }

 
render(){
  const closeBtn = <button className="close" onClick={this.toggle}>&times;</button>;   
    // RETURN THE COMPONENT
    return (
        <footer id="footer">
        <div className="footer__background" id="footerBackground"></div>
        <div className="footer">
           <h2 className="footer__title"> Hablemos<span className="merriweather">
              </span>
           </h2>
           <h3 className="footer__subtitle">No seas tímido</h3>
           <p className="footer__description"> Puese usar el <button className="underline underline--modal" onClick={this.toggle}>formulario </button> de arriba, o<a className="underline" href="mailto:christian@himmel.es">enviarme un e-mail </a>directamente si así lo preferís.
           </p>
           
          <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.clase}>
          <ModalHeader toggle={this.toggle} close={closeBtn}>Modal title</ModalHeader>
          <ModalBody>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
            dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
            ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
            fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
            mollit anim id est laborum.
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.toggle}>Do Something</Button>{' '}
            <Button color="secondary" onClick={this.toggle}>Cancel</Button>
          </ModalFooter>
        </Modal> 
        </div>
        <div className="footer__copyright">
           <div className="footer__row">
              <div className="footer__column footer__column--left">
                 <p>© Himmel.es / All rights reserved. </p>
              </div>
              <div className="footer__column">
                 <a className="underline underline--fix" href="mailto:info@himmel.es">info@himmel.es</a>
              </div>
              <div className="footer__column footer__column--right">
                 <p>Código disponible en<a className="underline" href="https://github.com/juanferreras/juanferreras.com">Github</a></p>
              </div>
           </div>
        </div>
     </footer>
     
     


    );
  }
}

export default Footer;
