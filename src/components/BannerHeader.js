import React, { Component } from 'react';
import './skills.css';


class BannerHeader extends Component {
  constructor(props){
    super(props);
    this.state = {count : 0} 
    this.incrementCount = this.incrementCount.bind(this)
  }
  incrementCount(){
   this.setState((prevState, props) => ({
      count: prevState.count + 1
    }));
 
    console.log(this.state.count);
  }

componentDidMount() {
 this.incrementCount();
}

  render() {
    
     return (
		
      <div className="banner-area">
          <div className="container">
              <div className="row fullscreen align-items-center justify-content-between">
                  <div className="col-lg-12 col-md-12 col-sm-12 banner-left mt-5">
                      <h6> Construimos tu proyecto para que este sea una</h6>
                      <h1>realidad</h1>
                      <button color="primary" onClick={this.incrementCount}>Do Something</button>

                      <p>
                      Sea cual sea tu necesidad, la estudiamos y la hacemos realidad.
Creamos tu Web corporativa, soluciones intranet, microsites,
comercios electrónicos o catálogos virtuales
que te acercará más a tus potenciales usuarios o clientes.</p>
<p>
En este mundo globalizado y competitivo el diseño es fundamental.
Por este motivo ponemos a tu disposición nuestros mejores
diseñadores que darán forma a tu mensaje y lo acercaran a tus
clientes.</p>
<p>
Destaca sobre la competencia y busca ser el mejor. 
                      </p>
                      {/*<a href="#" className="primary-btn text-uppercase">discover now</a>*/}
                  </div>
                  <div className="col-lg-12 col-md-12 col-sm-12 banner-right align-self-end d-sm-block mt-5">
                  <div className="row">
{/* skills */}


<div id="skills" className="cvitae-section cvitae-skills">
   <div className="cvitae-container">
      <div className="cvitae-section-content">
         <div className="content-left">
            <h3 className="cvitae-section-title">Skills</h3>
            <ul className="hi-lights">
               <li>Application Development</li>
               <li>Service-Oriented Architecture</li>
               <li>Enterprise Implementations </li>
               <li>Software Development Lifecycle</li>
            </ul>
         </div>
         <div className="content-right">
            <div className="progress-bar-container">
               <div className="progress">
                  <h3 className="progress-title">WordPress</h3>
                  <div className="progress-bar" data-progress="85">
                  <span className="bar wp">
                  </span>
                  <span className="text">85</span>
                  </div>
               </div>
               <div className="progress">
                  <h3 className="progress-title">HTML / CSS</h3>
                  <div className="progress-bar" data-progress="72">
                  <span className="bar wp">
                  </span>
                  <span className="text">72</span>
                  </div>
               </div>
               <div className="progress">
                  <h3 className="progress-title">JAVASCRIPT</h3>
                  <div className="progress-bar" data-progress="65">
                  <span className="bar wp">
                  </span>
                  <span className="text">65</span>
                  </div>
               </div>
               <div className="progress">
                  <h3 className="progress-title">PHP7</h3>
                  <div className="progress-bar" id="php" data-progress="69">
                  <span className="bar" id="phpBar">
                  </span>
                  <span className="text">69</span>
                  </div>
               </div>
               <div className="progress">
                  <h3 className="progress-title">jQuery</h3>
                  <div className="progress-bar" data-progress="78">
                  <span className="bar wp">
                  </span>
                  <span className="text">78</span>
                  </div>
               </div>
               <div className="progress">
                  <h3 className="progress-title">Laravel</h3>
                  <div className="progress-bar" data-progress="75">
                  <span className="bar wp">
                  </span>
                  <span className="text">75</span>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>


{/* skills */}
                  <div className="col-lg-12 col-md-12 col-sm-12 d-flex align-self-end d-sm-none">
                  <div id="myCanvasContainer">
      <canvas width="600" height="600" id="myCanvas">
        <p>Anything in here will be replaced on browsers that support the canvas element</p>
      </canvas>
    </div>
    <div id="tags">
      <ul>
      <li><a href="http://localhost:3000/">JavaScript</a></li>
<li><a href="http://localhost:3000/">Angular</a></li>
<li><a href="http://localhost:3000/">NodeJS</a></li>
<li><a href="http://localhost:3000/">PHP</a></li>
<li><a href="http://localhost:3000/">WordPress</a></li>
<li><a href="http://localhost:3000/">ReactJS</a></li>
<li><a href="http://localhost:3000/">Redux</a></li>
<li><a href="http://localhost:3000/">SCSS</a></li>
<li><a href="http://localhost:3000/">LESS</a></li>
<li><a href="http://localhost:3000/">HTML5</a></li>
<li><a href="http://localhost:3000/">JSX</a></li>
<li><a href="http://localhost:3000/">mongoDB</a></li>
<li><a href="http://localhost:3000/">MySQL</a></li>
<li><a href="http://localhost:3000/">Presstashop</a></li>
<li><a href="http://localhost:3000/">BootsStrap</a></li>
<li><a href="http://localhost:3000/">BEM</a></li>
<li><a href="http://localhost:3000/">JSON</a></li>
<li><a href="http://localhost:3000/">Gulp</a></li>
<li><a href="http://localhost:3000/">Grunt</a></li>
<li><a href="http://localhost:3000/">npm</a></li>
<li><a href="http://localhost:3000/">Git</a></li>
<li><a href="http://localhost:3000/">WebPack</a></li>
<li><a href="http://localhost:3000/">Drupal</a></li>
<li><a href="http://localhost:3000/">Express</a></li>
<li><a href="http://localhost:3000/">LAMP</a></li>
<li><a href="http://localhost:3000/">MEAN Stack</a></li>
      </ul>
    </div>
    </div>
    </div>
                      {/*<a href="index.html"><img src={logo} alt="logo" /></a>*/}
                      </div>

              </div>
          </div>         
		  </div>
    )
  }
  
}

export default BannerHeader;
